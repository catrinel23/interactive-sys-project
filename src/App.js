import React, { Component } from 'react';
import { BrowserRouter, Route } from 'react-router-dom';
import HomePage from './views/home-page/HomePage';
import QuizPage from './views/quiz-page/QuizPage';
import ResultsPage from './views/results-page/ResultsPage';
import LearnPage from './views/learn-page/LearnPage';
import ProfilePicture from './components/profile-picture/ProfilePicture';

import './App.scss';
import '../src/components/questions/basic-components/Common.scss';
import AvatarPage from './views/avatar-page/AvatarPage';

class App extends Component {
  componentWillMount() {
    let music = document.createElement("audio");
    document.body.appendChild(music);
    music.setAttribute("id", "pandaVoice");
  }

  render() {
    return (
      <div className="App">
        <ProfilePicture />
        <BrowserRouter>
        <div className="app-content">
          <Route exact path="/" component={AvatarPage} />  
          <Route exact path="/home" component={HomePage} />           
          <Route path="/quiz" component={QuizPage} />            
          <Route path="/results/:result" component={ResultsPage} /> 
          <Route path="/learn" component={LearnPage} />
        </div>           
        </BrowserRouter>
      </div>
    );
  }
}

export default App;
