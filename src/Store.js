export function getAvatar() {
    if (window.localStorage.avatar) {
        return window.localStorage.avatar;
    }
    return null;
}

export function getName() {
    if (window.localStorage.name) {
        return window.localStorage.name;
    }
    return null;
}

export function storeAvatar(avatar, name) {
    window.localStorage.avatar = avatar;
    window.localStorage.name = name;
}

export function resetAvatar() {
    window.localStorage.clear();
}