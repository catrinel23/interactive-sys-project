import React, { Component } from 'react';
import LeftArrow from '../../components/left-arrow/LeftArrow';

import './ResultsPage.scss';
import { getName } from '../../Store';
import { playFinalCuLin, playFinalFaraLin, playAgain } from '../../components/questions/basic-components/Common';

class ResultsPage extends Component {

  componentDidMount () {
    // const name = getName();
    const { result } = this.props.match.params;
    // let data = "\n" + `${name},${result},${new Date().toLocaleString()}`;

    if (result == 15) {
      playFinalCuLin();
      setTimeout(() => playAgain(), 6500);
    } else {
      playFinalFaraLin();
      setTimeout(() => playAgain(), 11500);
    }
  }

  render() {
    const { result } = this.props.match.params;
    const leaves = new Array(parseInt(result));
    leaves.fill(1);

    const top = 540;
    const left= 855;

    return (
      <div className="results-page">
        <LeftArrow redo={true} />
        <div className="results-container">
          <div className="pandalit" />
          { parseInt(result) === 15 && <div className="pandalita" />}
          <div className="basket" />
          <div className="leaves">
            {
              leaves.map((e, i) => {
                const arc = i%5 === 1 || i%5 === 3 ? 10 : (i%5 === 2 ? 15 : 0);
                return (<div className="bamboo-leaf" key={i}
                  style={{top: `${top - Math.trunc(i/5)*35 + arc}px`, left: `${left + (i%5)*30}px`, transform: `rotate(${i*2}deg)`, zIndex: `${5-Math.trunc(i/5)}`}}/>);
              })
            }
          </div>
          <div className="basket-2" />
        </div>
      </div>
    );
  }
}

export default ResultsPage;