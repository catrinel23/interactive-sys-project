import React, { Component } from 'react';
import Panda from '../../components/panda/Panda';
import Button from '../../components/button/Button';

import './HomePage.scss';
import { playHomePageAudio } from '../../components/questions/basic-components/Common';

class HomePage extends Component {

  _startQuiz = () => {
    window.location.pathname = 'quiz';
  }

  _redirectToLearnPage = () => {
    window.location.pathname = 'learn';
  }

  componentDidMount() {
    playHomePageAudio();
  }

  render() {
    return (
      <div className="home-page">
        <Panda onClick={this._redirectToLearnPage} onAudioClick={playHomePageAudio}/>
        <div className="home-container">
          <h1>Hai la joaca!</h1>
          <Button onClick={this._startQuiz} text="Incepem!" />
        </div>
      </div>
    );
  }
}

export default HomePage;