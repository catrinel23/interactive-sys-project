import React, { Component } from 'react';

import './AvatarPage.scss';
import Panda from '../../components/panda/Panda';
import { storeAvatar, resetAvatar } from '../../Store';
import { playAvatarPageAudio } from '../../components/questions/basic-components/Common';

class AvatarPage extends Component {
    names = [
        "TUDOR R.-A.",
        "ANAIS",
        "SARA-MARIA",
        "HOREA",
        "DARIUS",
        "SOPHIA C.",
        "ILINCA",
        "ANA B.",
        "MIHAI",
        "LORENA",
        "ANA D.",
        "ELENA",
        "VLAD",
        "SOFIA O.",
        "MATEI A.",
        "VICTOR",
        "AURORA",
        "JASMINE",
        "PATRICIA",
        "ANTONIA Ș.",
        "NATALIA",
        "MATEI P.",
        "ANDRA",
        "ANTONIA B.",
        "MATEI L.",
        "TUDOR M.-B."
    ];

    _chooseAvatar(index) {
        storeAvatar(`avatar-img-${index}`, this.names[index]);
        this._redirectToHomePage();
    }

    _redirectToHomePage = () => {
        window.location.pathname = 'home';
    }
    
    componentDidMount() {
        playAvatarPageAudio();
    }

    render() {
        let indexes = new Array(26);
        indexes.fill(1);

        return (
            <div className="avatar-page">
            <Panda onAudioClick={playAvatarPageAudio}/>
            <div className="avatar-container">
                <div>Alege-ti avatarul!</div>
                <div className="avatars">
                {
                   indexes.map((el, i) => <div className={`avatar avatar-${i}`} onClick={() => {
                    document.getElementsByClassName(`avatar avatar-${i}`)[0].classList.add('avatar-chosen');   
                    setTimeout(() => this._chooseAvatar(i), 2500);
                    }} ><div className={`avatar-img avatar-img-${i}`} /><div className="name">{this.names[i]}</div></div>)
                }
                </div>
            </div>
            </div>
        );
    }
}

export default AvatarPage;