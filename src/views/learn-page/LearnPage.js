import React, { Component } from 'react';
import Panda from '../../components/panda/Panda';
import Button from '../../components/button/Button';
import LeftArrow from '../../components/left-arrow/LeftArrow';

import './LearnPage.scss';

class LearnPage extends Component {

  render() {
    return (
      <div className="learn-page">
        <Panda />
        <LeftArrow />
        <div className="learn-container">
            <video width="848" height="480" controls autoPlay>
                <source src="panda.mp4" type="video/mp4" />
                Your browser does not support the video tag. 
            </video>
        </div>
      </div>
    );
  }
}

export default LearnPage;