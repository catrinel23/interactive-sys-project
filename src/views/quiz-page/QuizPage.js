import React, { Component } from 'react';
import RewardTree from '../../components/reward-tree/RewardTree';
import Panda from '../../components/panda/Panda';
import CeAnotimpUrmeaza from '../../components/questions/ce-anotimp-urmeaza/CeAnotimpUrmeaza';
import HainePentruAnotimp from '../../components/questions/haine-pentru-anotimp/HainePentruAnotimp';
import GrupLegume from '../../components/questions/grup-legume/GrupLegume';
import AnimaleApa from '../../components/questions/animale-apa/AnimaleApa';
import Zbor from '../../components/questions/zbor/Zbor';
import PuiOaie from '../../components/questions/pui-oaie/PuiOaie';
import AlegeFloarea from '../../components/questions/alege-floarea/AlegeFloarea';
import Pozitii from '../../components/questions/pozitii/Pozitii';
import MissingShape from '../../components/questions/missing-shape/MissingShape';
import ObiecteCamera from '../../components/questions/obiecte-camera/ObiecteCamera';
import Sequence from '../../components/questions/sequence/Sequence';
import OrderShapes from '../../components/questions/order-shapes/OrderShapes';
import Meserii from '../../components/questions/meserii/Meserii';
import AnimaleMancare from '../../components/questions/animale-mancare/AnimaleMancare';
import ObiectUmbra from '../../components/questions/obiect-umbra/ObiectUmbra';

import './QuizPage.scss';
import OrderPlant from '../../components/questions/order-plant/OrderPlant';
import { playFinish } from '../../components/questions/basic-components/Common';

class QuizPage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            currentQuestion: 0,
            pandaTopOffset: 590,
            pandaLeftOffset: 1050, 
            step: 40,
            correctAnswers: 0,
            orderArray: []
        };
    }

    componentDidMount() {
        let array = new Array(15);
        for (let i = 0; i < 15; i++) {
            array[i] = i;
        }

        this.setState({orderArray: this._shuffle(array)});
    }

    componentDidUpdate() {
        const { currentQuestion } = this.state;

        if (currentQuestion === 15) {
            playFinish();
            setTimeout(() => this._redirectToResults(), 2500);
        }
    }

    _shuffle(array) {
        var currentIndex = array.length, temporaryValue, randomIndex;
      
        // While there remain elements to shuffle...
        while (0 !== currentIndex) {
      
          // Pick a remaining element...
          randomIndex = Math.floor(Math.random() * currentIndex);
          currentIndex -= 1;
      
          // And swap it with the current element.
          temporaryValue = array[currentIndex];
          array[currentIndex] = array[randomIndex];
          array[randomIndex] = temporaryValue;
        }
      
        return array;
    }

    _nextQuestion = (nrTrials) => {
        const { currentQuestion, correctAnswers } = this.state;
        const quizContainer = document.getElementsByClassName("quiz-container")[0];
    
        let questionNr = currentQuestion + 1;

        quizContainer.classList.remove(`img${currentQuestion}`);
        quizContainer.classList.add(`img${questionNr}`);
        this.setState({currentQuestion: questionNr});

        if (nrTrials === 1 || nrTrials === 2) {
            this._climbPanda(nrTrials === 1);
            this.setState({correctAnswers: correctAnswers + (nrTrials === 1 ? 1 : 0)});
        }
    };

    _climbPanda = (correctAnswer) => {
        const {  pandaTopOffset, pandaLeftOffset, step } = this.state;
        const panda = document.getElementById("panda-container");
        let topOffset = pandaTopOffset;
        let leftOffset = pandaLeftOffset;
        let currentStep = correctAnswer ? step : step/2;

        if (topOffset <= 370) {
            if (topOffset >= 265) {
                leftOffset = Math.min(pandaLeftOffset + Math.min(currentStep/2, 15), 1075);
                topOffset = pandaTopOffset - currentStep;
            } else {
                if (topOffset <= 185) {
                    leftOffset = pandaLeftOffset - Math.min(30, currentStep);
                    topOffset = Math.min(pandaTopOffset - currentStep, 180);
                } else {
                    leftOffset = pandaLeftOffset - currentStep;
                    topOffset = Math.min(pandaTopOffset -  20, 220);
                }
            }
        } else {
            topOffset = pandaTopOffset - currentStep;
        }

        panda.style.left = `${leftOffset}px`;
        panda.style.top = `${topOffset}px`;
        this.setState({pandaTopOffset: topOffset, pandaLeftOffset: leftOffset});
    }

    _redirectToResults = () => {
        window.location.pathname = `results/${this.state.correctAnswers}`;
    }

    _getQuestions = () => {
        return [
            <GrupLegume wrongAnswer={() => this.wrongAnswer()} correctAnswer={(nrTrials) => this.correctAnswer(nrTrials)} />,
            <AnimaleApa wrongAnswer={() => this.wrongAnswer()} correctAnswer={(nrTrials) => this.correctAnswer(nrTrials)} />,
            <HainePentruAnotimp wrongAnswer={() => this.wrongAnswer()} correctAnswer={(nrTrials) => this.correctAnswer(nrTrials)} />,
            <Zbor wrongAnswer={() => this.wrongAnswer()} correctAnswer={(nrTrials) => this.correctAnswer(nrTrials)} />,
            <PuiOaie wrongAnswer={() => this.wrongAnswer()} correctAnswer={(nrTrials) => this.correctAnswer(nrTrials)} />,
            <AlegeFloarea wrongAnswer={() => this.wrongAnswer()} correctAnswer={(nrTrials) => this.correctAnswer(nrTrials)} />,
            <Pozitii wrongAnswer={() => this.wrongAnswer()} correctAnswer={(nrTrials) => this.correctAnswer(nrTrials)} />,
            <MissingShape wrongAnswer={() => this.wrongAnswer()} correctAnswer={(nrTrials) => this.correctAnswer(nrTrials)} />,
            <ObiecteCamera wrongAnswer={() => this.wrongAnswer()} correctAnswer={(nrTrials) => this.correctAnswer(nrTrials)} />,
            <Sequence wrongAnswer={() => this.wrongAnswer()} correctAnswer={(nrTrials) => this.correctAnswer(nrTrials)} />,
            <OrderPlant wrongAnswer={() => this.wrongAnswer()} correctAnswer={(nrTrials) => this.correctAnswer(nrTrials)} />,
            <OrderShapes wrongAnswer={() => this.wrongAnswer()} correctAnswer={(nrTrials) => this.correctAnswer(nrTrials)} />,
            <Meserii wrongAnswer={() => this.wrongAnswer()} correctAnswer={(nrTrials) => this.correctAnswer(nrTrials)} />,
            <AnimaleMancare wrongAnswer={() => this.wrongAnswer()} correctAnswer={(nrTrials) => this.correctAnswer(nrTrials)} />,
            <CeAnotimpUrmeaza wrongAnswer={() => this.wrongAnswer()} correctAnswer={(nrTrials) => this.correctAnswer(nrTrials)} />,
            <ObiectUmbra wrongAnswer={() => this.wrongAnswer()} correctAnswer={(nrTrials) => this.correctAnswer(nrTrials)} />
        ];              
    }

    _getQuestion = (index) => {
        return this._getQuestions()[index];
    }

    correctAnswer = (nrTrials) => {
        setTimeout(() => this._nextQuestion(nrTrials), 500);
    }

    wrongAnswer = () => {
        setTimeout(() => this._nextQuestion(0), 500);
    }

    render() {
        return (
            <div className="quiz-page"> 
                <div className="quiz-container">
                    <div className="quiz-question">
                        {this.state.currentQuestion === 0 && this._getQuestion(this.state.orderArray[0])}
                        {this.state.currentQuestion === 1 && this._getQuestion(this.state.orderArray[1])}
                        {this.state.currentQuestion === 2 && this._getQuestion(this.state.orderArray[2])}
                        {this.state.currentQuestion === 3 && this._getQuestion(this.state.orderArray[3])}
                        {this.state.currentQuestion === 4 && this._getQuestion(this.state.orderArray[4])}
                        {this.state.currentQuestion === 5 && this._getQuestion(this.state.orderArray[5])}
                        {this.state.currentQuestion === 6 && this._getQuestion(this.state.orderArray[6])}
                        {this.state.currentQuestion === 7 && this._getQuestion(this.state.orderArray[7])}
                        {this.state.currentQuestion === 8 && this._getQuestion(this.state.orderArray[8])}
                        {this.state.currentQuestion === 9 && this._getQuestion(this.state.orderArray[9])}
                        {this.state.currentQuestion === 10 && this._getQuestion(this.state.orderArray[10])}
                        {this.state.currentQuestion === 11 && this._getQuestion(this.state.orderArray[11])}
                        {this.state.currentQuestion === 12 && this._getQuestion(this.state.orderArray[12])}
                        {this.state.currentQuestion === 13 && this._getQuestion(this.state.orderArray[13])}
                        {this.state.currentQuestion === 14 && this._getQuestion(this.state.orderArray[14])}
                        
                        {this.state.currentQuestion === 15 && <div className="felicitari">Felicitari!</div>}
                    </div>
                </div>
                <Panda getFromStorage={true} />
                <RewardTree />
            </div>
        );
  }
}

export default QuizPage;