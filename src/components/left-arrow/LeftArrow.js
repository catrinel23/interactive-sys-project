import React, { Component } from 'react';

import './LeftArrow.scss';
import { resetAvatar } from '../../Store';

class LeftArrow extends Component {
    constructor(props) {
        super(props);
        this.state = {};
    }

    _redirectStart = () => {
        resetAvatar();
        window.location.pathname = "/";
    }

    _redirectHome = () => {
        window.location.pathname = "/home";
    }

    render() {
        const { redo, text } = this.props;
        return (
            redo ? <div className="redo" onClick={() => this._redirectStart()}>{text}</div> : 
            <div className="left-arrow" onClick={() => this._redirectHome()}>{text}</div>
        );
    }
}

export default LeftArrow;