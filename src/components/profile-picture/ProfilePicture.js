import React, { Component } from 'react';

import './ProfilePicture.scss';
import { getAvatar, getName } from '../../Store';

class ProfilePicture extends Component {
  constructor(props) {
    super(props);
    this.state = {
      avatar: null, 
      name: null
    };
  }

  _redirectHome = () => {
    window.location.pathname = "/home";
  }

  componentDidMount() {
    if (window.location.pathname === "/") {
      window.localStorage.clear();
      return;
    }
    const avatar = getAvatar();
    const name = getName();
    this.setState({avatar, name});
  }

  render() {
      return (
        this.state.avatar && 
            (<div className="profile-back">
              <div className={"profile-picture " + this.state.avatar} onClick={() => this._redirectHome()} />
              <div className="name">{this.state.name}</div>
            </div>)
      );
  }
}

export default ProfilePicture;