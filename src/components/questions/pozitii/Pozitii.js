import React, { Component } from 'react';
import MultipleAnswer from '../basic-components/multiple-answer/MultipleAnswer';

import './Pozitii.scss';
import SingleAnswer from '../basic-components/single-answer/SingleAnswer';

class Pozitii extends Component {
    options = [
    {
        class: 'pozitii-vazapemasa',
        onClick: () => this.props.wrongAnswer(),
        isCorrect: false
    },
    {
        class: 'pozitii-radioPeJos',
        onClick: () => this.props.wrongAnswer(),
        isCorrect: false
    },
    {
        class: 'pozitii-pisicaSubMasa',
        onClick: () => this.props.wrongAnswer(),
        isCorrect: false
    },
    {
        class: 'pozitii-pantofSubMasa',
        onClick: (nrTrials) => this.props.correctAnswer(nrTrials),
        isCorrect: true,
    },
    {
        class: 'pozitii-mingeSubMasa',
        onClick: () => this.props.wrongAnswer(),
        isCorrect: false
    },
    {
        class: 'pozitii-laptopPemasa',
        onClick: () => this.props.wrongAnswer(),
        isCorrect: false
    },
    ];

    render() {
        return (
            <div className="pozitii">
                <SingleAnswer options={this.options} text="Ce se afla atat sub masa cat si in stanga pisicii?" image={"pozitii-masa"}
                audioFile={"IntrebareSubMasa.wav"} audioTime={4} correctDuration={1} correctFile={"Corect.wav"}
                mistakeHintDuration={4} mistakeHintFile={"incaOSansa.wav"} mistakeNextDuration={4} mistakeNextFile={"raspunsGresit.wav"}
                expNextDuration={1} expHintDuration={1} />
            </div>
        );
    }
}

export default Pozitii;