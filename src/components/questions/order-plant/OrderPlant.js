import React, { Component } from 'react';

import './OrderPlant.scss';
import Cronological from '../basic-components/cronological/Cronological';

class OrderPlant extends Component {
    options = [
    {
        class: 'order-plant-5',
        onClick: (nrTrials) => this.props.correctAnswer(nrTrials),
        isCorrect: true,
        position: 3
    },
    {
        class: 'order-plant-6',
        onClick: (nrTrials) => this.props.correctAnswer(nrTrials),
        isCorrect: true,
        position: 4
    },
    // {
    //     class: 'order-plant-1',
    //     onClick: (nrTrials) => this.props.correctAnswer(nrTrials),
    //     isCorrect: true,
    //     position: 1
    // },
    {
        class: 'order-plant-3',
        onClick: (nrTrials) => this.props.correctAnswer(nrTrials),
        isCorrect: true,
        position: 1
    },
    {
        class: 'order-plant-4',
        onClick: (nrTrials) => this.props.correctAnswer(nrTrials),
        isCorrect: true,
        position: 2
    },
    {
        class: 'order-plant-7',
        onClick: (nrTrials) => this.props.correctAnswer(nrTrials),
        isCorrect: true,
        position: 5
    },
    ];

    render() {
        return (
            <div className="order-plant">
                <Cronological options={this.options} text="Ordoneaza evolutia plantei" nrAnswers={5} wrongAnswer={() => this.props.wrongAnswer()}
                audioFile={"IntrebarePlanta.mp3"} audioTime={3} correctDuration={3} correctFile={"Felicitari.wav"}
                mistakeHintDuration={4} mistakeHintFile={"incaOSansa.wav"} mistakeNextDuration={4} mistakeNextFile={"raspunsGresit.wav"}
                expNextDuration={1} expHintDuration={1} />
            </div>
        );
    }
}

export default OrderPlant;