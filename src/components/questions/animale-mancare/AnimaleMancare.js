import React, { Component } from 'react';
import Pairs from '../basic-components/pairs/Pairs';

import './AnimaleMancare.scss';

class AnimaleMancare extends Component {
    optionsLeft = [
    {
        class: 'animale-mancare-caine1',
        isTop: true,
        code: 1
    },
    {
        class: 'animale-mancare-vaca1',
        isTop: true,
        code: 3
    },
    {
        class: 'animale-mancare-gaina1',
        isTop: true,
        code: 2
    },
    ];

    optionsRight = [
    {
        class: 'animale-mancare-vaca2',
        isTop: false,
        code: 3,
    },
    {
        class: 'animale-mancare-caine2',
        isTop: false,
        code: 1
    },
    {
        class: 'animale-mancare-gaina2',
        isTop: false,
        code: 2
    },
    ];

    render() {
        return (
            <div className="animale-mancare">
                <Pairs optionsLeft={this.optionsLeft} optionsRight={this.optionsRight} text="Uneste animalul cu mancarea" 
                    nrAnswers={3} wrongAnswer={() => this.props.wrongAnswer()} correctAnswer={(nrTrials) => this.props.correctAnswer(nrTrials)}
                    audioFile={"IntrebareMancareAnimal.wav"} audioTime={7} correctDuration={4} correctFile={"raspunsCorect.wav"}
                    mistakeHintDuration={4} mistakeHintFile={"incaOSansa.wav"} mistakeNextDuration={4} mistakeNextFile={"raspunsGresit.wav"}
                    expNextDuration={1} expHintDuration={1} />
            </div>
        );
    }
}

export default AnimaleMancare;