import React, { Component } from 'react';
import SingleAnswer from '../basic-components/single-answer/SingleAnswer';

import './CeAnotimpUrmeaza.scss';

class CeAnotimpUrmeaza extends Component {
    options = [
    {
        class: 'anotimp-primavara',
        onClick: (nrTrials) => this.props.correctAnswer(nrTrials),
        isCorrect: true
    },
    {
        class: 'anotimp-iarna',
        onClick: () => this.props.wrongAnswer(),
        isCorrect: false
    },
    {
        class: 'anotimp-toamna',
        onClick: () => this.props.wrongAnswer(),
        isCorrect: false
    }];

    render() {
        return (
            <div className="ce-anotimp-urmeaza">
                <SingleAnswer options={this.options} text="Ce anotimp urmeaza dupa iarna?" 
                audioFile={"IntrebareAnotimp.wav"} audioTime={2} correctDuration={1} correctFile={"Corect.wav"}
                mistakeHintDuration={4} mistakeHintFile={"incaOSansa.wav"} mistakeNextDuration={4} mistakeNextFile={"raspunsGresit.wav"}
                expNextDuration={1} expHintDuration={1} />
            </div>
        );
    }
}

export default CeAnotimpUrmeaza;