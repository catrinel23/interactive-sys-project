import React, { Component } from 'react';
import SingleAnswer from '../basic-components/single-answer/SingleAnswer';

import './PuiOaie.scss';

class PuiOaie extends Component {
    options = [
    {
        class: 'pui-oaie-vaca',
        onClick: () => this.props.wrongAnswer(),
        isCorrect: false
    },
    {
        class: 'pui-oaie-pui',
        onClick: (nrTrials) => this.props.correctAnswer(nrTrials),
        isCorrect: true,
    },
    {
        class: 'pui-oaie-capra',
        onClick: () => this.props.wrongAnswer(),
        isCorrect: false
    },];

    render() {
        return (
            <div className="pui-oaie">
                <SingleAnswer options={this.options} text="Care este puiul oii?" 
                audioFile={"IntrebarePuiulOii.wav"} audioTime={4} correctDuration={4} correctFile={"raspunsCorect.wav"}
                mistakeHintDuration={4} mistakeHintFile={"incaOSansa.wav"} mistakeNextDuration={4} mistakeNextFile={"raspunsGresit.wav"}
                expNextDuration={1} expHintDuration={1} />
            </div>
        );
    }
}

export default PuiOaie;