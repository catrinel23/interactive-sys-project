import React, { Component } from 'react';

import './ObiecteCamera.scss';
import MultipleAnswer from '../basic-components/multiple-answer/MultipleAnswer';

class ObiecteCamera extends Component {
    options = [
    {
        class: 'obiecte-camera-dulap',
        onClick: () => this.props.wrongAnswer(),
        isCorrect: false
    },
    {
        class: 'obiecte-camera-wc',
        onClick: (nrTrials) => this.props.correctAnswer(nrTrials),
        isCorrect: true,
    },
    {
        class: 'obiecte-camera-canapea',
        onClick: () => this.props.wrongAnswer(),
        isCorrect: false
    },
    {
        class: 'obiecte-camera-chiuveta',
        onClick: (nrTrials) => this.props.correctAnswer(nrTrials),
        isCorrect: true,
    },
    {
        class: 'obiecte-camera-semineu',
        onClick: () => this.props.wrongAnswer(),
        isCorrect: false
    },
    {
        class: 'obiecte-camera-frigider',
        onClick: () => this.props.wrongAnswer(),
        isCorrect: false
    },
    {
        class: 'obiecte-camera-biblioteca',
        onClick: () => this.props.wrongAnswer(),
        isCorrect: false
    },
    {
        class: 'obiecte-camera-masa',
        onClick: () => this.props.wrongAnswer(),
        isCorrect: false
    },
    {
        class: 'obiecte-camera-pat',
        onClick: () => this.props.wrongAnswer(),
        isCorrect: false
    },
    {
        class: 'obiecte-camera-televizor',
        onClick: () => this.props.wrongAnswer(),
        isCorrect: false
    },
    {
        class: 'obiecte-camera-dus',
        onClick: (nrTrials) => this.props.correctAnswer(nrTrials),
        isCorrect: true,
    },
    {
        class: 'obiecte-camera-aragaz',
        onClick: () => this.props.wrongAnswer(),
        isCorrect: false
    },
    ];

    render() {
        return (
            <div className="obiecte-camera">
                <MultipleAnswer options={this.options} text="Ce obiecte se gasesc in baie?" nrAnswers={3} 
                 audioFile={"IntrebareObiecteBaie.wav"} audioTime={3} correctDuration={4} correctFile={"raspunsCorect.wav"}
                 mistakeHintDuration={4} mistakeHintFile={"incaOSansa.wav"} mistakeNextDuration={4} mistakeNextFile={"raspunsGresit.wav"}
                 expNextDuration={1} expHintDuration={1} />
            </div>
        );
    }
}

export default ObiecteCamera;