
export function playMistakeHint(file) {
    let music = document.getElementById("pandaVoice");
    music.setAttribute('src', file ||'mistake-hint.wav');
    playAudio(music, 900);
}

export function playMistakeNext(file) {
    let music = document.getElementById("pandaVoice");
    music.setAttribute('src', file ||'mistake-next.wav');
    playAudio(music, 900);
}

export function playExpHint(file) {
    let music = document.getElementById("pandaVoice");
    music.setAttribute('src', file ||'GataTimpMaiIncearca.wav');
    playAudio(music);
}

export function playExpNext(file) {
    let music = document.getElementById("pandaVoice");
    music.setAttribute('src', file ||'GataTimpUrmatoarea.wav');
    playAudio(music);
}

export function playCorrect(file) {
    let music = document.getElementById("pandaVoice");
    music.setAttribute('src', file || 'corect.wav');
    playAudio(music);
}

export function playAvatarPageAudio() {
    let music = document.getElementById("pandaVoice");
    music.setAttribute('src', 'AlegeAvatar.wav');
    playAudio(music);
}

export function playHomePageAudio() {
    let music = document.getElementById("pandaVoice");
    music.setAttribute('src', 'Intro.wav');
    playAudio(music);
}

export function playFinalCuLin() {
    let music = document.getElementById("pandaVoice");
    music.setAttribute('src', 'FinalCuLinna.wav'); 
    playAudio(music);
}

export function playFinalFaraLin() {
    let music = document.getElementById("pandaVoice");
    music.setAttribute('crossorigin', 'anonymous');  
    music.setAttribute('src', 'FinalFaraLinna.wav');
    playAudio(music);
}

export function playQuestionAudio(filename) {
    localStorage.setItem("questionFile", filename);
    let music = document.getElementById("pandaVoice");
    music.setAttribute('src', filename);
    playAudio(music);
}

export function playFinish() {
    let music = document.getElementById("pandaVoice");
    music.setAttribute('src', 'FelicitariAiTerminatTestul.wav');
    playAudio(music);
}

export function playAgain() {
    let music = document.getElementById("pandaVoice");
    music.setAttribute('src', 'DaTestulIar.wav');
    playAudio(music);
}

function playAudio(music) {
    music.addEventListener('loadeddata', () => {
        let duration = music.duration;
        music.play();

        let overlay = document.createElement('div');
        overlay.classList.add('overlay-disable-clicks');
        document.body.appendChild(overlay);
        setTimeout(() => {
            document.body.removeChild(overlay);
        }, duration * 1000 + 500);
    });
    music.load();
};