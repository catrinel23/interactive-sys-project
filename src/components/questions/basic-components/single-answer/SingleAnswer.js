import React, { Component } from 'react';
import PandaHelper from '../../../panda-helper/PandaHelper';

import './SingleAnswer.scss';
import { playMistakeHint, playExpHint, playMistakeNext, playExpNext, playCorrect, playQuestionAudio } from '../Common';

class SingleAnswer extends Component {
    constructor(props) {
        super(props);
        this.state = {
            nrTries: 1, 
            hasHint: false,
            options: this.props.options,
            timeout: 47000
        }
    }

    timeoutFunction = null;
    intervalFunction = null;
    // textNode = document.createTextNode('0');

    componentDidMount() {
        const { audioFile, audioTime } = this.props;
        playQuestionAudio(audioFile || 'corect.wav');

        setTimeout(() => {
            // document.getElementById('seconds').appendChild(this.textNode);

            this.timeoutFunction = setTimeout(() => this._timeOut(), this.state.timeout);
            // this._setInterval();
        }, audioTime || 2000);
    }

    componentWillUnmount() {
        clearTimeout(this.timeoutFunction);
        clearInterval(this.intervalFunction);
    }

    _onAnswer = (option) => {
        const { nrTries } = this.state;
        const { correctDuration, mistakeHintDuration, mistakeNextDuration,
            correctFile, mistakeHintFile, mistakeNextFile} = this.props;
       
        if (option.isCorrect) {
            playCorrect(correctFile);
            clearTimeout(this.timeoutFunction);
            clearInterval(this.intervalFunction);

            option._isClicked = true;
            this.setState({nrTries: nrTries + 1});

            setTimeout(() => {
                option.onClick(nrTries);
            }, correctDuration*1000 || 7000);
            
            return;
        }
        
        if (this.state.nrTries === 1) {
            playMistakeHint(mistakeHintFile);
            this._mistake(option, mistakeHintDuration || 11);
        } else {
            playMistakeNext(mistakeNextFile);
            this._mistake(option, mistakeNextDuration || 7);
        }        
    }

    _timeOut = () => {
        if (this.state.nrTries === 1) {
            playExpHint();
            this._mistake(null, 6);
        } else {
            playExpNext();
            this._mistake(null, 4);
        }
    }

    // _setInterval = () => {
    //     this.intervalFunction = setInterval((function (){
    //         var start = Date.now();
    //         return () => {
    //             this.textNode.data = Math.floor((Date.now()-start)/1000);
    //         };
    //     }).bind(this)(), 1000);
    // }

    _mistake = (option, seconds) => {
        const { nrTries, options } = this.state;   

        clearTimeout(this.timeoutFunction);
        clearInterval(this.intervalFunction);

        let el;
        if (option) {
            el = document.getElementsByClassName(option.class)[0];
            el.classList.add("wrong-answer");
        }

        setTimeout(() => {
            if (option) el.classList.remove("wrong-answer");

            if (nrTries === 1) {
                this._hint(option);
            } else {
                if (option) {
                    option.onClick();
                }
                else {
                    options.find(option => !option.isCorrect).onClick();
                }
            }
            
        }, 900 + seconds*1000);
    }

    _hint = (option) => {
        const { nrTries, options, timeout } = this.state;
        let index;

        if (option && !option.isCorrect) {
            index = options.findIndex(o => o.class === option.class);
        } else {
            do {
                index = Math.floor(Math.random()*options.length);
            } while(options[index].isCorrect);
        }
        
        options.splice(index, 1);
        this.setState({nrTries: nrTries + 1, hasHint: true, options, timeout: timeout - 15000});
        this.timeoutFunction = setTimeout(() => this._timeOut(), this.state.timeout);
        // this._setInterval();
    }

    render() {
        return (
            <div className="single-answer">
                { this.state.hasHint && <PandaHelper /> }
                { this.props.image && 
                    <div className="question"> 
                        <div className={`q-image ${this.props.image}`} />
                    </div>
                }
                <div className="options">
                {   
                    this.state.options.map(option => 
                        <div key={option.class} className={`option ${option.class} ${option._isClicked && 'given'}`}
                            onClick={() => this._onAnswer(option)}/>
                    )
                }
                </div>

            </div>
        );
    }
}

export default SingleAnswer;