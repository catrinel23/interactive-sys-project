import React, { Component } from 'react';
import PandaHelper from '../../../panda-helper/PandaHelper';
import { playMistakeHint, playExpHint, playMistakeNext, playExpNext, playCorrect, playQuestionAudio } from '../Common';

import './Pairs.scss';

class Pairs extends Component {
    constructor(props) {
        super(props);
        this.state = {
            nrTries: 1, 
            hasHint: false,
            optionsLeft: this.props.optionsLeft,
            optionsRight: this.props.optionsRight,
            givenAnswers: 1, 
            timeout: 47000,
            activeOption: null
        }
    }

    timeoutFunction = null;
    intervalFunction = null;
    // textNode = document.createTextNode('0');

    componentDidMount() {
        const { audioFile, audioTime } = this.props;
        playQuestionAudio(audioFile || 'corect.wav');

        setTimeout(() => {
            // document.getElementById('seconds').appendChild(this.textNode);

            this.timeoutFunction = setTimeout(() => this._timeOut(), this.state.timeout);
            // this._setInterval();
        }, audioTime || 2000);
    }

    componentWillUnmount() {
        clearTimeout(this.timeoutFunction);
        clearInterval(this.intervalFunction);
    }

    _onAnswer = (option) => {
        const { nrTries, givenAnswers, activeOption } = this.state;
        const { correctDuration, mistakeHintDuration, mistakeNextDuration,
            correctFile, mistakeHintFile, mistakeNextFile} = this.props;
       
        if (option._isClicked) {
            return;
        }

        if (!activeOption) {
            this.setState({activeOption: option});
            option._isClicked = true;
            return;
        }


        if (option.code === activeOption.code) {
            if (givenAnswers === this.props.nrAnswers) { 
                playCorrect(correctFile);
                clearTimeout(this.timeoutFunction);
                clearInterval(this.intervalFunction);

                option._isClicked = true;
                this.setState({givenAnswers: givenAnswers + 1});
               
                setTimeout(() => {
                    this.props.correctAnswer(nrTries);
                }, correctDuration*1000 || 7000);
                
                return;
            }
            this.setState({givenAnswers: givenAnswers + 1, activeOption: null});
            option._isClicked = true;
            return;
        }

        if (this.state.nrTries === 1) {
            playMistakeHint(mistakeHintFile);
            this._mistake(option, mistakeHintDuration || 11);
        } else {
            playMistakeNext(mistakeNextFile);
            this._mistake(option, mistakeNextDuration || 7);
        }   
    }

    _timeOut = () => {
        if (this.state.nrTries === 1) {
            playExpHint();
            this._mistake(null, 6);
        } else {
            playExpNext();
            this._mistake(null, 4);
        }
    }

    // _setInterval = () => {
    //     this.intervalFunction = setInterval((function (){
    //         var start = Date.now();
    //         return () => {
    //             this.textNode.data = Math.floor((Date.now()-start)/1000);
    //         };
    //     }).bind(this)(), 1000);
    // }

    _mistake = (option, seconds) => {
        const { nrTries, activeOption } = this.state;

        clearTimeout(this.timeoutFunction);
        clearInterval(this.intervalFunction);

        let el1, el2;
        if (option) {
            el1 = document.getElementsByClassName(option.class)[0];
            el2 = document.getElementsByClassName(activeOption.class)[0];
            el1.classList.add("wrong-answer");
            el2.classList.add("wrong-answer");
        }

        setTimeout(() => {
            if (option) {
                el1.classList.remove("wrong-answer");
                el2.classList.remove("wrong-answer");
            }

            if (nrTries === 1) {
                this._hint(option);
            } else {
                this.props.wrongAnswer();
            }
            
        }, 900 + seconds*1000);
    }

    _hint = (option) => {
        const { nrTries, optionsLeft, optionsRight, timeout, givenAnswers, activeOption } = this.state;

        let givenA;
        if (activeOption) activeOption._isClicked = false;
        
        givenA = givenAnswers + 1;
        this._selectFirstAnswer(option);

        this.setState({nrTries: nrTries + 1, hasHint: true, optionsLeft, optionsRight, timeout: timeout - 15000, givenAnswers: givenA, activeOption: null});
        this.timeoutFunction = setTimeout(() => this._timeOut(), this.state.timeout);
        // this._setInterval();
    }

    _selectFirstAnswer = (option) => {
        const { optionsLeft, optionsRight } = this.state;
        let index, bottomIndex;
        if (option) {
            index = optionsLeft.findIndex(o => o.code === option.code);
            bottomIndex = optionsRight.findIndex(o => o.code === option.code);
        } else {
            do {
                index = Math.floor(Math.random()*(optionsLeft.length));
                bottomIndex = optionsRight.findIndex(option => option.code === optionsLeft[index].code);
            } while(optionsLeft[index]._isClicked);
        }

        optionsLeft[index]._isClicked = true;
        optionsRight[bottomIndex]._isClicked = true;
    }

    render() {
        return (
            <div className="pairs">
                {this.state.hasHint && <PandaHelper />}
                {/* <div id="seconds">{this.props.text}</div> */}
                <div className="options">
                    <div className="optionsTop">
                    {   
                        this.state.optionsLeft.map(option => 
                            <div className={`option ${option.class} ${option._isClicked && `given-${option.code}`}`} key={option.class}
                                onClick={() => this._onAnswer(option)} 
                            ></div>
                        )
                    }
                    </div>
                    <div className="optionsBottom">
                    {   
                        this.state.optionsRight.map(option => 
                            <div className={`option ${option.class} ${option._isClicked && `given-${option.code}`}`} key={option.class}
                                onClick={() => this._onAnswer(option)}
                            ></div>
                        )
                    }
                    </div>
                </div>
            </div>
        );
    }

}

export default Pairs;