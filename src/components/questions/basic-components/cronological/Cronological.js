import React, { Component } from 'react';
import PandaHelper from '../../../panda-helper/PandaHelper';
import { playMistakeHint, playExpHint, playMistakeNext, playExpNext, playCorrect, playQuestionAudio } from '../Common';

import './Cronological.scss';

class Cronological extends Component {
    constructor(props) {
        super(props);
        this.state = {
            nrTries: 1, 
            hasHint: false,
            options: this.props.options,
            givenAnswers: 1, 
            timeout: 47000
        }
    }

    timeoutFunction = null;
    intervalFunction = null;
    // textNode = document.createTextNode('0');

    componentDidMount() {
        const { audioFile, audioTime } = this.props;
        playQuestionAudio(audioFile || 'corect.wav');

        setTimeout(() => {
            // document.getElementById('seconds').appendChild(this.textNode);

            this.timeoutFunction = setTimeout(() => this._timeOut(), this.state.timeout);
            // this._setInterval();
        }, audioTime || 2000);
    }
    
    componentWillUnmount() {
        clearTimeout(this.timeoutFunction);
        clearInterval(this.intervalFunction);
    }

    _onAnswer = (option) => {
        const { nrTries, givenAnswers } = this.state;
        const { correctDuration, mistakeHintDuration, mistakeNextDuration,
            correctFile, mistakeHintFile, mistakeNextFile } = this.props;
       
        if (option._isClicked) {
            return;
        }

        if (option.isCorrect && option.position === givenAnswers) {
            if (givenAnswers === this.props.nrAnswers) {
                playCorrect(correctFile);
                clearTimeout(this.timeoutFunction);
                clearInterval(this.intervalFunction);
                
                option._isClicked = true; 
                this.setState({givenAnswers: givenAnswers + 1});
                setTimeout(() => {
                    option.onClick(nrTries);
                }, correctDuration*1000 || 7000);
                return;
            }
            this.setState({givenAnswers: givenAnswers + 1});
            option._isClicked = true;
            return;
        }

        if (this.state.nrTries === 1) {
            playMistakeHint(mistakeHintFile);
            this._mistake(option, mistakeHintDuration || 11);
        } else {
            playMistakeNext(mistakeNextFile);
            this._mistake(option, mistakeNextDuration || 7);
        }   
    }

    _timeOut = () => {
        if (this.state.nrTries === 1) {
            playExpHint();
            this._mistake(null, 6);
        } else {
            playExpNext();
            this._mistake(null, 4);
        }
    }

    // _setInterval = () => {
    //     this.intervalFunction = setInterval((function (){
    //         var start = Date.now();
    //         return () => {
    //             this.textNode.data = Math.floor((Date.now()-start)/1000);
    //         };
    //     }).bind(this)(), 1000);
    // }

    _mistake = (option, seconds) => {
        const { nrTries } = this.state;

        clearTimeout(this.timeoutFunction);
        clearInterval(this.intervalFunction);

        let el;
        if (option) {
            el = document.getElementsByClassName(option.class)[0];
            el.classList.add("wrong-answer");
        }

        setTimeout(() => {
            if (option) el.classList.remove("wrong-answer");

            if (nrTries === 1) {
                this._hint(option);
            } else {
                if (option) option.onClick();
                else this.props.wrongAnswer();
            }
            
        }, 900 + seconds*1000);
    }

    _hint = (option) => {
        const { nrTries, options, timeout, givenAnswers } = this.state;
        let index;

        if (option && !option.isCorrect) {
            index = options.findIndex(o => o.class === option.class);
            options.splice(index, 1);
        } else {
            if (options.length !== this.props.nrAnswers) {
                do {
                    index = Math.floor(Math.random()*options.length);
                } while(options[index].isCorrect);
                options.splice(index, 1);
            }
            
        }
        

        let givenA;
        if (givenAnswers > 1) {
            givenA = givenAnswers;
        } else {
            options.forEach(element => {
                element._isClicked = false; // givenAns : 2
            });
            this._selectFirstAnswer();
            givenA = 2;
        }

        this.setState({nrTries: nrTries + 1, hasHint: true, options, timeout: timeout - 15000, givenAnswers: givenA});
        this.timeoutFunction = setTimeout(() => this._timeOut(), this.state.timeout);
        // this._setInterval();
    }

    _selectFirstAnswer = () => {
        const { options } = this.state;
        options.find(element => element.position === 1)._isClicked = true;
    }

    render() {
        return (
            <div className="cronological">
                {this.state.hasHint && <PandaHelper />}
                {/* <p id="seconds">{this.props.text}</p> */}
                <div className="options">
                {   
                    this.state.options.map(option => 
                        <div className={`option ${option.class} ${option._isClicked && 'given'}`} key={option.class}
                            onClick={() => this._onAnswer(option)}
                        >
                            {option._isClicked && <div className={`number number-${option.position}`}/>}
                        </div>
                    )
                }
                </div>

            </div>
        );
    }
}

export default Cronological;