import React, { Component } from 'react';
import Pairs from '../basic-components/pairs/Pairs';

import './Meserii.scss';

class Meserii extends Component {
    optionsLeft = [
    {
        class: 'meserii-policeman1',
        isTop: true,
        code: 1
    },
    {
        class: 'meserii-firefighter1',
        isTop: true,
        code: 2
    },
    {
        class: 'meserii-bucatar1',
        isTop: true,
        code: 3
    },
    {
        class: 'meserii-doctor1',
        isTop: true,
        code: 4
    },
    ];

    optionsRight = [
    {
        class: 'meserii-doctor2',
        isTop: false,
        code: 4
    },
    {
        class: 'meserii-bucatar2',
        isTop: false,
        code: 3,
    },
    {
        class: 'meserii-policeman2',
        isTop: false,
        code: 1
    },
    {
        class: 'meserii-firefighter2',
        isTop: false,
        code: 2
    },
    ];

    render() {
        return (
            <div className="meserii">
                <Pairs optionsLeft={this.optionsLeft} optionsRight={this.optionsRight} text="Uneste meseria cu unealta" 
                    nrAnswers={4} wrongAnswer={() => this.props.wrongAnswer()} correctAnswer={(nrTrials) => this.props.correctAnswer(nrTrials)}
                    audioFile={"IntrebaeMeserii.wav"} audioTime={6} correctDuration={1} correctFile={"Foarte bine.wav"}
                    mistakeHintDuration={4} mistakeHintFile={"incaOSansa.wav"} mistakeNextDuration={4} mistakeNextFile={"raspunsGresit.wav"}
                    expNextDuration={1} expHintDuration={1} />
            </div>
        );
    }
}

export default Meserii;