import React, { Component } from 'react';
import SingleAnswer from '../basic-components/single-answer/SingleAnswer';

import './Sequence.scss';

class Sequence extends Component {
    options = [
    {
        class: 'sequence-opt-2',
        onClick: (nrTrials) => this.props.correctAnswer(nrTrials),
        isCorrect: true,
    },
    {
        class: 'sequence-opt-4',
        onClick: () => this.props.wrongAnswer(),
        isCorrect: false
    },
    {
        class: 'sequence-opt-3',
        onClick: () => this.props.wrongAnswer(),
        isCorrect: false
    },
    {
        class: 'sequence-opt-1',
        onClick: () => this.props.wrongAnswer(),
        isCorrect: false
    },
    
    ];

    render() {
        return (
            <div className="sequence">
                <SingleAnswer options={this.options} text="Continua sirul cu varianta corecta" image={"sequence-question"} 
                audioFile={"IntrebareContinuareSir.wav"} audioTime={3} correctDuration={3} correctFile={"Felicitari.wav"}
                mistakeHintDuration={4} mistakeHintFile={"incaOSansa.wav"} mistakeNextDuration={4} mistakeNextFile={"raspunsGresit.wav"}
                expNextDuration={1} expHintDuration={1} />
            </div>
        );
    }
}

export default Sequence;