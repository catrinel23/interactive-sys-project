import React, { Component } from 'react';
import SingleAnswer from '../basic-components/single-answer/SingleAnswer';

import './HainePentruAnotimp.scss';

class HainePentruAnotimp extends Component {
    options = [{
        class: 'imbracaminte-anotimp-iarna',
        onClick: () => this.props.wrongAnswer(),
        isCorrect: false
    },
    {
        class: 'imbracaminte-anotimp-primavara',
        onClick: () => this.props.wrongAnswer(),
        isCorrect: false
    },
    {
        class: 'imbracaminte-anotimp-toamna',
        onClick: (nrTrials) => this.props.correctAnswer(nrTrials),
        isCorrect: true,
    },
    {
        class: 'imbracaminte-anotimp-vara',
        onClick: () => this.props.wrongAnswer(),
        isCorrect: false
    },];

    render() {
        return (
            <div className="haine-pentru-anotimp">
                <SingleAnswer options={this.options} text="Cum ne imbracam in anotimpul din imagine?" image={"anotimp-toamna"}
                audioFile={"IntrebareHaine.wav"} audioTime={5} correctDuration={1} correctFile={"Foarte bine.wav"}
                mistakeHintDuration={4} mistakeHintFile={"incaOSansa.wav"} mistakeNextDuration={4} mistakeNextFile={"raspunsGresit.wav"}
                expNextDuration={1} expHintDuration={1} />
            </div>
        );
    }
}

export default HainePentruAnotimp;