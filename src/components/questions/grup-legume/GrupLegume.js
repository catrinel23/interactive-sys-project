import React, { Component } from 'react';
import SingleAnswer from '../basic-components/single-answer/SingleAnswer';

import './GrupLegume.scss';

class GrupLegume extends Component {
    options = [
    {
        class: 'grup-legume-rosii',
        onClick: (nrTrials) => this.props.correctAnswer(nrTrials),
        isCorrect: true,
    },
    {
        class: 'grup-legume-ardei',
        onClick: () => this.props.wrongAnswer(),
        isCorrect: false
    },
    {
        class: 'grup-legume-lamaie',
        onClick: () => this.props.wrongAnswer(),
        isCorrect: false
    },
    {
        class: 'grup-legume-morcovi',
        onClick: () => this.props.wrongAnswer(),
        isCorrect: false
    },
    {
        class: 'grup-legume-dovleac',
        onClick: () => this.props.wrongAnswer(),
        isCorrect: false
    },
    {
        class: 'grup-legume-banana',
        onClick: () => this.props.wrongAnswer(),
        isCorrect: false
    },
    {
        class: 'grup-legume-porumb',
        onClick: () => this.props.wrongAnswer(),
        isCorrect: false
    },
    {
        class: 'grup-legume-ridiche',
        onClick: () => this.props.wrongAnswer(),
        isCorrect: false
    },
    ];

    render() {
        return (
            <div className="grup-legume">
                <SingleAnswer options={this.options} text="Care legume sunt grupate cate 3?" 
                audioFile={"IntrebareLegume.wav"} audioTime={3} correctDuration={1} correctFile={"Foarte bine.wav"}
                mistakeHintDuration={4} mistakeHintFile={"incaOSansa.wav"} mistakeNextDuration={4} mistakeNextFile={"raspunsGresit.wav"}
                expNextDuration={1} expHintDuration={1} />
            </div>
        );
    }
}

export default GrupLegume;