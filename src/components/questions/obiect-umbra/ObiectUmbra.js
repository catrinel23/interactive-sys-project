import React, { Component } from 'react';
import Pairs from '../basic-components/pairs/Pairs';

import './ObiectUmbra.scss';

class ObiectUmbra extends Component {
    optionsLeft = [
    {
        class: 'obiect-umbra-a1',
        isTop: true,
        code: 1
    },
    {
        class: 'obiect-umbra-b1',
        isTop: true,
        code: 2
    },
    {
        class: 'obiect-umbra-c1',
        isTop: true,
        code: 3
    },
    {
        class: 'obiect-umbra-d1',
        isTop: true,
        code: 4
    },
    {
        class: 'obiect-umbra-e1',
        isTop: true,
        code: 5
    },
    {
        class: 'obiect-umbra-g1',
        isTop: true,
        code: 6
    },
    {
        class: 'obiect-umbra-f1',
        isTop: true,
        code: 7
    },
    ];

    optionsRight = [
    {
        class: 'obiect-umbra-f2',
        isTop: false,
        code: 7
    },
    {
        class: 'obiect-umbra-c2',
        isTop: false,
        code: 3,
    },
    {
        class: 'obiect-umbra-a2',
        isTop: false,
        code: 1
    },
    {
        class: 'obiect-umbra-b2',
        isTop: false,
        code: 2
    },
    {
        class: 'obiect-umbra-g2',
        isTop: true,
        code: 6
    },
    {
        class: 'obiect-umbra-e2',
        isTop: true,
        code: 5
    },
    {
        class: 'obiect-umbra-d2',
        isTop: true,
        code: 4
    },
    ];

    render() {
        return (
            <div className="obiect-umbra">
                <Pairs optionsLeft={this.optionsLeft} optionsRight={this.optionsRight} text="Uneste dinozaurul cu umbra lui" 
                    nrAnswers={7} wrongAnswer={() => this.props.wrongAnswer()} correctAnswer={(nrTrials) => this.props.correctAnswer(nrTrials)}
                    audioFile={"IntrebareDinozauri.wav"} audioTime={11} correctDuration={4} correctFile={"raspunsCorect.wav"}
                    mistakeHintDuration={4} mistakeHintFile={"incaOSansa.wav"} mistakeNextDuration={4} mistakeNextFile={"raspunsGresit.wav"}
                    expNextDuration={1} expHintDuration={1} />
            </div>
        );
    }
}

export default ObiectUmbra;