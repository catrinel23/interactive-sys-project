import React, { Component } from 'react';
import SingleAnswer from '../basic-components/single-answer/SingleAnswer';

import './AlegeFloarea.scss';

class AlegeFloarea extends Component {
    options = [
    {
        class: 'alege-floarea-lalalea',
        onClick: () => this.props.wrongAnswer(),
        isCorrect: false
    },
    {
        class: 'alege-floarea-rose',
        onClick: () => this.props.wrongAnswer(),
        isCorrect: false
    },
    
    {
        class: 'alege-floarea-floareasoarelui',
        onClick: () => this.props.wrongAnswer(),
        isCorrect: false
    },
    {
        class: 'alege-floarea-ghiocel',
        onClick: (nrTrials) => this.props.correctAnswer(nrTrials),
        isCorrect: true,
    },
    {
        class: 'alege-floarea-nuStiuCeEDarECute',
        onClick: () => this.props.wrongAnswer(),
        isCorrect: false
    },
    {
        class: 'alege-floarea-lotus',
        onClick: () => this.props.wrongAnswer(),
        isCorrect: false
    },
    ];

    render() {
        return (
            <div className="alege-floarea">
                <SingleAnswer options={this.options} text="Da click pe ghiocel" 
                audioFile={"IntrebareGhiocel.wav"} audioTime={3} correctDuration={1} correctFile={"Corect.wav"}
                mistakeHintDuration={4} mistakeHintFile={"incaOSansa.wav"} mistakeNextDuration={4} mistakeNextFile={"raspunsGresit.wav"}
                expNextDuration={1} expHintDuration={1} />
            </div>
        );
    }
}

export default AlegeFloarea;