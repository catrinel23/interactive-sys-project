import React, { Component } from 'react';

import './OrderShapes.scss';
import Cronological from '../basic-components/cronological/Cronological';

class OrderShapes extends Component {
    options = [
    {
        class: 'order-shapes-circle-1',
        onClick: () => this.props.wrongAnswer(),
        isCorrect: false
    },
    {
        class: 'order-shapes-triangle-2',
        onClick: (nrTrials) => this.props.correctAnswer(nrTrials),
        isCorrect: true,
        position: 2
    },
    {
        class: 'order-shapes-triangle-no-4',
        onClick: () => this.props.wrongAnswer(),
        isCorrect: false
    },
    {
        class: 'order-shapes-triangle-1',
        onClick: (nrTrials) => this.props.correctAnswer(nrTrials),
        isCorrect: true,
        position: 1
    },
    {
        class: 'order-shapes-square-3',
        onClick: () => this.props.wrongAnswer(),
        isCorrect: false
    },
    {
        class: 'order-shapes-square-4',
        onClick: () => this.props.wrongAnswer(),
        isCorrect: false
    },
    {
        class: 'order-shapes-triangle-3',
        onClick: (nrTrials) => this.props.correctAnswer(nrTrials),
        isCorrect: true,
        position: 3
    },
    {
        class: 'order-shapes-triangle-no-1',
        onClick: () => this.props.wrongAnswer(),
        isCorrect: false
    },
    {
        class: 'order-shapes-triangle-4',
        onClick: (nrTrials) => this.props.correctAnswer(nrTrials),
        isCorrect: true,
        position: 4
    },
    ];

    render() {
        return (
            <div className="order-shapes">
                <Cronological options={this.options} text="Ordoneaza descrescator triunghiurile verzi" nrAnswers={4} wrongAnswer={() => this.props.wrongAnswer()} 
                audioFile={"IntrebareTriunghiuri.wav"} audioTime={4} correctDuration={3} correctFile={"Felicitari.wav"}
                mistakeHintDuration={4} mistakeHintFile={"incaOSansa.wav"} mistakeNextDuration={4} mistakeNextFile={"raspunsGresit.wav"}
                expNextDuration={1} expHintDuration={1} />
            </div>
        );
    }
}

export default OrderShapes;