import React, { Component } from 'react';

import './Zbor.scss';
import MultipleAnswer from '../basic-components/multiple-answer/MultipleAnswer';

class Zbor extends Component {
    options = [
    {
        class: 'zbor-pinguin',
        onClick: () => this.props.wrongAnswer(),
        isCorrect: false
    },
    {
        class: 'zbor-vrabie',
        onClick: (nrTrials) => this.props.correctAnswer(nrTrials),
        isCorrect: true,
    },
    {
        class: 'zbor-copil',
        onClick: () => this.props.wrongAnswer(),
        isCorrect: false
    },
    
    {
        class: 'zbor-avion',
        onClick: (nrTrials) => this.props.correctAnswer(nrTrials),
        isCorrect: true,
    },
    {
        class: 'zbor-vapor',
        onClick: () => this.props.wrongAnswer(),
        isCorrect: false
    },
    {
        class: 'zbor-albina',
        onClick: (nrTrials) => this.props.correctAnswer(nrTrials),
        isCorrect: true,
    },
    {
        class: 'zbor-casa',
        onClick: () => this.props.wrongAnswer(),
        isCorrect: false
    },
    ];

    render() {
        return (
            <div className="zbor">
                <MultipleAnswer options={this.options} text="Cine poate sa zboare?" nrAnswers={3} 
                audioFile={"IntrebareZburat.wav"} audioTime={5} correctDuration={1} correctFile={"Corect.wav"}
                mistakeHintDuration={4} mistakeHintFile={"incaOSansa.wav"} mistakeNextDuration={4} mistakeNextFile={"raspunsGresit.wav"}
                expNextDuration={1} expHintDuration={1} />
            </div>
        );
    }
}

export default Zbor;