import React, { Component } from 'react';
import SingleAnswer from '../basic-components/single-answer/SingleAnswer';

import './MissingShape.scss';

class MissingShape extends Component {
    options = [
    {
        class: 'missing-shape-opt-1',
        onClick: () => this.props.wrongAnswer(),
        isCorrect: false
    },
    {
        class: 'missing-shape-opt-2',
        onClick: () => this.props.wrongAnswer(),
        isCorrect: false
    },
    
    {
        class: 'missing-shape-opt-3',
        onClick: () => this.props.wrongAnswer(),
        isCorrect: false
    },
    {
        class: 'missing-shape-opt-4',
        onClick: (nrTrials) => this.props.correctAnswer(nrTrials),
        isCorrect: true,
    },
    {
        class: 'missing-shape-opt-5',
        onClick: () => this.props.wrongAnswer(),
        isCorrect: false
    },
    {
        class: 'missing-shape-opt-6',
        onClick: () => this.props.wrongAnswer(),
        isCorrect: false
    },
    ];

    render() {
        return (
            <div className="missing-shape">
                <SingleAnswer options={this.options} text="Care piesa completeaza imaginea?" image={"missing-shape-question"} 
                audioFile={"IntrebareImagineLipsa.wav"} audioTime={2} correctDuration={1} correctFile={"Foarte bine.wav"}
                mistakeHintDuration={4} mistakeHintFile={"incaOSansa.wav"} mistakeNextDuration={4} mistakeNextFile={"raspunsGresit.wav"}
                expNextDuration={1} expHintDuration={1} />
            </div>
        );
    }
}

export default MissingShape;