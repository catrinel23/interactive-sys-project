import React, { Component } from 'react';

import './AnimaleApa.scss';
import MultipleAnswer from '../basic-components/multiple-answer/MultipleAnswer';

class AnimaleApa extends Component {
    options = [
    {
        class: 'animale-apa-dog',
        onClick: () => this.props.wrongAnswer(),
        isCorrect: false
    },
    {
        class: 'animale-apa-whale',
        onClick: (nrTrials) => this.props.correctAnswer(nrTrials),
        isCorrect: true,
    },
    {
        class: 'animale-apa-octopus',
        onClick: (nrTrials) => this.props.correctAnswer(nrTrials),
        isCorrect: true,
    },
    {
        class: 'animale-apa-cow',
        onClick: () => this.props.wrongAnswer(),
        isCorrect: false
    },
    {
        class: 'animale-apa-horse',
        onClick: () => this.props.wrongAnswer(),
        isCorrect: false
    },
    {
        class: 'animale-apa-rooster',
        onClick: () => this.props.wrongAnswer(),
        isCorrect: false
    },
    {
        class: 'animale-apa-fish',
        onClick: (nrTrials) => this.props.correctAnswer(nrTrials),
        isCorrect: true,
    },
    ];

    render() {
        return (
            <div className="animale-apa">
                <MultipleAnswer options={this.options} text="Care dintre urmatoarele animale traiesc in apa?" nrAnswers={3} 
                    audioFile={"IntrebareAnimaleApa.mp3"} audioTime={4} correctDuration={3} correctFile={"Felicitari.wav"}
                    mistakeHintDuration={4} mistakeHintFile={"incaOSansa.wav"} mistakeNextDuration={4} mistakeNextFile={"raspunsGresit.wav"}
                    expNextDuration={1} expHintDuration={1} />
            </div>
        );
    }
}

export default AnimaleApa;