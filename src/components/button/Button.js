import React, { Component } from 'react';

import './Button.scss';

class Button extends Component {
    constructor(props) {
        super(props);
        this.state = {};
    }
    render() {
        const { onClick, text } = this.props;
        return (
            <div className="button-default" onClick={() => onClick()}></div>
        );
    }
}

export default Button;