import React, { Component } from 'react';

import './Panda.scss';
import { playQuestionAudio } from '../questions/basic-components/Common';

class Panda extends Component {

  _getHandler = () => {
    if (this.props.getFromStorage) {
      const fileName = localStorage.getItem("questionFile");
      playQuestionAudio(fileName);
      return;
    }
    this.props.onAudioClick();
  };

  render() {
      return (
        <div id="panda-container">
          <div className="chat-head"><div className="voice" onClick={this._getHandler}/></div>
          <div className="panda" onClick={this.props.onClick}/>
        </div>
      );
  }
}

export default Panda;